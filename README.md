# README #

## ZG Hackweek July 2020 Project - Favorite Home Sheet##
A tool to export data to a spreadsheet to help analyze all your favorite homes in one consolidated view. 

### Description ###
* When looking to buy or rent a house, many of us prefer to perform in-depth research before we make a decision. And what tool is more useful than our humble little spreadsheet! A spreadsheet provides a tabular format of data thus giving us one consolidated view that is easy to compare and glance over.
* Let me give you a little background here to tell you what is the pain point that we intend to solve with this project. When I relocated for work, I went to zillow.com and typed in the region of my office location. After looking at so many options, I picked all that I liked marking them as favorites. Then I went into the "Saved Homes" (aka Favorites) section and to my disappointment found a long list of saved homes that I had to go over again to choose just one! To my surprise, the favorite section is paginated with separate tiles of information per home and I just wasn't able to effectively compare my options! So I did what most of us do. Open a blank spreadsheet on my computer, created various columns of my interest like address, price, area, the price per sqft, etc. and then started copy-pasting the data present on the favorite section on to my blank spreadsheet. Too tedious. 
* Well, lo and behold, now that's no more a problem with our project to the rescue! The idea is to have an "Export to spreadsheet" button on the favorite section which will enable us to download all the saved homes data in the form of a spreadsheet, thus automating that tedious manual task! 
* We have also thought about giving the customer, the power to customize their spreadsheet! 
	* They can select/deselect columns of their interest before they could download the spreadsheet. 
	* There is also an option to enter a location from which you will get a column in the spreadsheet called "Distance". This is the distance of every home from the entered location in miles. Generally, people look at buying/renting a house when they have a purpose to relocate, like near to office for work or to grandma's to help her, etc. So knowing the distance from a place helps to prioritize home search as per our needs.

### Future Scope ###
The future scope of this project as envisioned by us is to have this spreadsheet-like tabular formatted data on the website itself. The benefits are three-fold.

1. No longer a hassle to download a spreadsheet every time you mark a new home as favorite. The tabular formatted data on the website will be updated in real-time.
2. Enable sharing of this table on the website to whoever has a Zillow account. That way we increase our user base as well. All the people who have this favorite section shared will be able to edit/view changes in real-time.
3. Enable having multiple favorite lists, so that the user can decide which one to share. This will give the user more fine-grained control over their research. One use case is, this will help people who are looking to rent a house currently but are also preparing to buy a house in the future, helping them create two separate favorite lists.

**This is a simple idea yet very powerful, for it helps you to take that important decision in your life of "what you call home".**

### How do I get set up? ###
We have written a Greasemonkey script to showcase our idea that works on https://www.zillow.com/myzillow/favorites. The script is named "export-favorite-homes.user.js" in the same directory as this README.md. Follow the below steps to install the script:

1. Download [greasemonkey add-on](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/) for your firefox browser.
2. After a successful installation, you will see a monkey icon on the top-right navigation bar of your firefox browser. Click on that icon and select "New user script..." from the drop-down. A new tab will open.
3. Copy the contents of the "export-favorite-homes.user.js" file from this bitbucket repo and paste them on the newly opened tab by Greasemonkey add-on in your firefox browser. Hit the save button on the top-left corner of the side-bar or press "Ctrl"+"S" ("Cmd" + "S" for Mac) on your keyboard.
4. Go to [zillow.com](https://www.zillow.com/), sign in, and mark your favorite homes. 
5. Go to the ["Saved Homes"](https://www.zillow.com/myzillow/favorites) section. You will see an "Export to spreadsheet" button on the top-right corner.
5. Click on the "Export to spreadsheet" button. You will get a pop-up box - "Customize your spreadsheet" to select/deselect the columns you want in your spreadsheet. You may also add in a location in the "Distance from" field in the pop-up box to get the distance of every home from the entered location in the spreadsheet.
6. Hit "Download". Save the ".xlsx" file on to your filesystem and happy analyzing your favorite homes.

#### Few caveats of writing a Greasemonkey script instead of modifying the actual source code ####
* The favorite section is paginated and the html for every page loads as you visit that page. We could only grab the data present on the first page since our data comes from parsing html content. Hence, for now, the generated spreadsheet can only have homes listed on the first page of the favorite section.
* Please scroll all the way down on the first page of the favorite section before you click on the "Export to spreadsheet" button. The reason, the html on the page loads only as the parts of the page become visible to the user. We need the DOM of the entire page to be loaded to grab data for all the homes listed on the first page.

### Wondering why Greasemonkey script and not edit the actual zillow.com source code? ###

We recently joined Zillow as full time engineers and did not have the frontend set up on our machines. 

#### Why not setup frontend on your machine then? ####
I have tried setting up the frontend dev environment on my machine previously. Invested a lot of time, mine as well as senior devs on my team, but couldn't get the frontend up and running :( There is some issue on my laptop which I need to figure out for the frontend setup to happen. Could not invest any more time in the setup especially during the hack week and hence decided on Plan B. Write a Greasemonkey script.

Sorry for the inconvenience caused.
Thank you for your time!

### More questions? Who do I talk to? ###

* Repo owner - @neelamv or @chethanr on slack

*Happy home shopping!*