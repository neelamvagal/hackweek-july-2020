// ==UserScript==
// @name        Favorite_Home_Sheet
// @namespace   https://www.zillow.com/myzillow/favorites
// @description Export favorites data to spreadsheet to analyze in one consolidated view
// @author      Neelam Vagal, Chethan Setty
// @include     https://www.zillow.com/myzillow/favorites*
// @version     1
// @require     https://code.jquery.com/jquery-3.5.1.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js
// @require     https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js
// @require     https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.16.3/xlsx.mini.min.js
// @require     http://cdn.jsdelivr.net/g/filesaver.js
// @require     https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.js
// @require     https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.min.js
// ==/UserScript==

$(document).ready(function(){
  $("body").append('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">');
  $("body").append('<link  rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/v1.11.1/mapbox-gl.css" />');
  $("body").append('<link rel="stylesheet" href="https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v4.5.1/mapbox-gl-geocoder.css" type="text/css" />');
         
  var exportElement = '<div id="exportContainer"><button class="exportButton" data-toggle="modal" data-target="#exportModal">Export to spreadsheet</button></div>';
  var modalElement = `
                <div class="modal fade" id="exportModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Customize your spreadsheet</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body" id="modalBody">
                          <div id="columnCheckboxes"></div>
                          <div class="form-group row location-container">
                            <label for="location-input" class="col-sm-4 col-form-label">Distance from</label>
                            <div class="col-sm-8" id="location-input"></div>
                          </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="downloadButton">Download</button>
                      </div>
                    </div>
                  </div>
                </div>`;
  
  //------------- Add export button and modal to UI -------------
  $("h1[class^='FavoritesList__StyledHeader']").after(exportElement);
  $("body").append(modalElement);
  
  
  //------------- Fill modal body with checkboxes -------------
  var columnNames = new Set();
  columnNames.add("Price");
  columnNames.add("Details");
  columnNames.add("Address");
  columnNames.add("Link");
  allLabels = $('[class*="StyledLabel"]');
  for(var label of allLabels) {
    columnNames.add(label.textContent.replace("/", " Per ").replace(/[\W_]+/g,""));
  }
  columnList = [];
  for (var col of columnNames){
    columnList.push(col);
  }
  columnList.sort();
  
  createModalBody(columnList);
  
  function createModalBody(columnList) {
    var selectAll = `
                <div class="form-check select-all">
                  <input class="form-check-input"type="checkbox" value="" id="selectAll" checked>
                  <label class="form-check-label" for="selectAll">Select all</label>
                </div>`;
    $("#columnCheckboxes").append(selectAll);

    $.each(columnList, function(index, value) {
        var checkbox = `
                    <div class="form-check">
                      <input class="form-check-input column-checkbox" type="checkbox" value="" id="`+value+`" checked>
                      <label class="form-check-label" for="`+value+`">`+value+`</label>
                    </div>`;
        $("#columnCheckboxes").append(checkbox);
    });


    $("#selectAll").click(function () {
          $(".column-checkbox").prop('checked', $(this).prop('checked'));
      });

      $(".column-checkbox").change(function() {
          if (!$(this).prop("checked")) {
              $("#selectAll").prop("checked",false);
          }
    });
  }
  //------------- Location autocomplete -------------
  
  mapboxgl.accessToken = 'pk.eyJ1IjoibGxhbWFmdW5kdWNrIiwiYSI6ImNrY29hcXlqODA5eGkyd29iZjRwYjF2cnMifQ.TNHXHYpCugsCYRc1squSIw';
  var geocoder = new MapboxGeocoder({
    accessToken: mapboxgl.accessToken
  });
  var locationCoordinates = [];
  geocoder.addTo('#location-input');
  geocoder.on('result', function(res) {
      locationCoordinates = res.result.geometry.coordinates;
  });
  
  
  //------------- SheetJS -------------
  var wb = XLSX.utils.book_new();
  wb.Props = {
                Title: "Favorite Homes",
                Subject: "Export Data",
                Author: "NC",
                CreatedDate: new Date(2020,07,14)
        };

  wb.SheetNames.push("Home Sheet"); 
  
  function s2ab(s) { 
      var buf = new ArrayBuffer(s.length);                             //convert s to arrayBuffer
      var view = new Uint8Array(buf);                                  //create uint8array as viewer
      for (var i=0; i<s.length; i++) view[i] = s.charCodeAt(i) & 0xFF; //convert to octet
      return buf;    
  };
  
  
  function calDistance(lat1, lon1, lat2, lon2) {
      var p = 0.017453292519943295;    // Math.PI / 180
      var c = Math.cos;
      var a = 0.5 - c((lat2 - lat1) * p)/2 + 
              c(lat1 * p) * c(lat2 * p) * 
              (1 - c((lon2 - lon1) * p))/2;

      return 7918 * Math.asin(Math.sqrt(a)); // 2 * R; R = 3959 km
  }
  
  function pushColumnData(columnName, columnValue, columnArray) {
      if ($('#'+columnName).prop("checked")) {
        columnArray.push(columnValue);
      }
  }

  
  function populateDataInSheet(ws_data, locationValues) {
      var divs = document.querySelectorAll('div[class*="FavoritesList__StyledPropertyListCard"]');
      var i = 0;
      for (var div of divs) {
            var labelMap = {}
            var col = []
            var price = div.querySelectorAll('div[class="list-card-price"]')[0].textContent;
            var detailsUi = div.querySelectorAll('[class="list-card-details"]');
            var details = ""
            for (var li of detailsUi[0].childNodes) {
              details += li.textContent;
            }
            var address = div.querySelectorAll('[class="list-card-addr"]')[0].textContent;
            var link = div.querySelectorAll('[class="list-card-link"]')[0].href;
            
            if (locationValues != undefined) {
                var homeCoordinates = locationValues[i].features[0].geometry.coordinates;
                var distance = calDistance(locationCoordinates[0],locationCoordinates[1],homeCoordinates[0],homeCoordinates[1]).toFixed(2);
                col.push(distance);
            }
        
            var labels = div.querySelectorAll('[class*="StyledLabel"]');
            labelMap["Price"] = price;
            labelMap["Details"] = details;
            labelMap["Address"] = address;
            labelMap["Link"] = link;
            for (var label of labels) {
               labelMap[label.textContent.replace("/", " Per ").replace(/[\W_]+/g,"")] = label.nextSibling.textContent;
            }
            for( var column of columnList) {
              if ( labelMap[column] != null) {
                pushColumnData(column, labelMap[column], col);
              } else {
                pushColumnData(column, "-", col);
              }
            }
            ws_data.push(col);
            
            i+=1;
       }
    
    return ws_data;
  }
  
  function populateData(ws_data, isLocationGiven) {
      var divs = document.querySelectorAll('div[class*="FavoritesList__StyledPropertyListCard"]');
    
      if (isLocationGiven) {
          var distancePromises = [];
          for (var div of divs) {
            var address = div.querySelectorAll('[class="list-card-addr"]')[0].textContent;
            var encodedAddress = encodeURI(address);
            var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+encodedAddress+'.json?country=US&access_token=pk.eyJ1IjoibGxhbWFmdW5kdWNrIiwiYSI6ImNrY29hcXlqODA5eGkyd29iZjRwYjF2cnMifQ.TNHXHYpCugsCYRc1squSIw';
            distancePromises.push($.ajax(url));

          }

          return Promise.all(distancePromises).then((values) => {
            return populateDataInSheet(ws_data, values);
          }).catch(console.log);
      }
    
    return new Promise( (resolve,reject) => {
      resolve(populateDataInSheet(ws_data));
    });
  }
  
  function populateHeaders(ws_data, isLocationGiven) {
      var header = [];
      if (isLocationGiven) {
          header.push("Distance");
      }
      $.each(columnList, function(index, value) {
          pushColumnData(value, value, header);
      });
      ws_data.push(header);
      return ws_data;
  }
  
  //------------- Handle button click action -------------
  $("#downloadButton").click(function() {
      var location = $('.mapboxgl-ctrl-geocoder--input').val();
      isLocationGiven = !!location.trim();
      $('#exportModal').modal('hide');
      var ws_data = [];
      ws_data = populateHeaders(ws_data, isLocationGiven)
      populateData(ws_data, isLocationGiven).then((ws_data) => {
        var ws = XLSX.utils.aoa_to_sheet(ws_data);
        wb.Sheets["Home Sheet"] = ws;
        var wbout = XLSX.write(wb, {bookType:'xlsx',  type: 'binary'});
        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), 'home-data.xlsx');
      });  
  });
  

  //------------- Style elements using CSS -------------
  function addCss(cssString) {
      var head = document.getElementsByTagName('head')[0];
      var newCss = document.createElement('style');
      newCss.type = "text/css";
      newCss.innerHTML = cssString;
      head.appendChild(newCss);
  }
  addCss(`
      #exportContainer {
          display:                inline-block;
          float:                  right;
      }
      .exportButton {
          background-color:       rgb(255, 255, 255);
          color:                  rgb(0, 106, 255);
          border-color:           rgb(0, 106, 255);
          border-radius:          4px;
          border:                 1px solid;
          padding:                9px 16px;
          font-weight:            700;
          font-size:              16px;
      }
      .select-all {
          border-bottom:          1px solid #e9ecef;
      }
      .form-check:nth-child(2) {
          padding-top:            1rem;
      }
      .iWNUme {
          display:                inline-block;
      }
      .location-container {
          padding-left:           1.25rem;
      }
  `);
})